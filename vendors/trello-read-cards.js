// CONVERTOR DE TEXTO
var converter = new showdown.Converter();

// ACCORDION
function accordion(element){
  // SE O ACCORDION ESTIVER ABERTO
  if($(element).next().css("display") == 'block'){
    $(element).removeClass('active');
    $('.body_accordion').slideUp("fast");
  }else{
  // SE O ACCORDION ESTIVER FECHADO
    $(".item_accordion").removeClass('active');
    $('.body_accordion').slideUp("fast");
    $(element).addClass('active');
    $(element).next().slideToggle("fast");
  }
};

function getAwsers(){
  var data = null;
  var textAction;
  var xhr = new XMLHttpRequest();
  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === this.DONE) {
      data = JSON.parse(this.responseText);
      var id_label = "";
      $.each(data, function(i, item) {
        // GET VÍDEO ID
        for(var i=0; i<item['labels'].length; i++){
          id_label = id_label+" "+item['labels'][i]['id'];
        }
        // IMPRIMIR PERGUNTAS E RESPOSTAS
        $("#accordions").hide().append('<div class="accordion margin-bottom" data-search-term="'+item['name']+'" data-custom-type="'+id_label+'"><div class="item_accordion" onclick="accordion(this)"><div class="col-md-10 col-sm-10 col-xs-10 accordion_pergunta"><p>'+item['name']+'</p></div><div class="col-md-2 col-sm-2 col-xs-2 accordion_arrow"><span class="fa fa-angle-down"></span></div></div><div class="body_accordion">'+converter.makeHtml(item['desc'])+'</div></div>').slideDown(500);
        id_label = "";
      });
    }
  });

  xhr.open("GET", "https://api.trello.com/1/lists/5cd9b2c5df0b525eddbbf5e7/cards?fields=id,name,labels,desc,descData,actions&key=f173ab582acacc165addb2dee7c21220&token=");
  
  xhr.send(data);  

}

getAwsers();

function getLabels(){
  var data = null;

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === this.DONE) {
      data = JSON.parse(this.responseText);
      $.each(data, function(i, item) {
          $("#categorias").append('<option value="'+item['id']+'" class="text-uppercase">'+item['name']+'</option>')
      });
    }
  });

  xhr.open("GET", "https://api.trello.com/1/boards/5cd9b2a3f37fac466f0a007c/labels?fields=all&limit=50&key=f173ab582acacc165addb2dee7c21220&token=");

  xhr.send(data);
}

getLabels();