jQuery(document).ready(function($){

  $('.accordion_pergunta p').each(function(){
    $(this).attr('data-search-term', $(this).text().toLowerCase());
  });

  $('select#categorias').change(function() {
    var filter = $(this).val();
    filterList(filter);
  });

  function filterList(value) {
    var list = $(".accordion");
    $(list).hide();
    if (value == "All") {
      $(".accordion").each(function (i) {
        $(this).show();
      });
    } else {
      $(".lista-perguntas").find("div[data-custom-type*=" + value + "]").each(function (i) {
        $(this).show();
      });
    }
  }
  
  $(document).ready(function(){
      $("#search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#accordions .accordion").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
  });

  $(".botaocampo").click(function(){
    location.reload(true);
  });

});